import React from 'react';

const Jokers = (props) => {
    return (
        <div style={{width: '65%',padding: '10px', margin: '50px auto', border: '1px solid black'}}>
            <h1>{props.joke}</h1>
        </div>
    );
};

export default Jokers;